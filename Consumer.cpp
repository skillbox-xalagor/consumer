#include <iostream>
#include <thread>
#include <string>
#include <chrono>
#include <mutex>
#include <ctime>
#include <vector>
#include <condition_variable>
#include <random>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include <windows.h>

using namespace std;

// ������ �������
enum class CSVState {
	UnquotedField,
	QuotedField,
	QuotedQuote
};

vector<string> readCSVRow(const string& row) {
	CSVState state = CSVState::UnquotedField;
	vector<string> fields{ "" };
	size_t i = 0; // index of the current field
	for (char c : row) {
		switch (state) {
		case CSVState::UnquotedField:
			switch (c) {
			case ',': // end of field
				fields.push_back(""); i++;
				break;
			case '"': state = CSVState::QuotedField;
				break;
			default:  fields[i].push_back(c);
				break;
			}
			break;
		case CSVState::QuotedField:
			switch (c) {
			case '"': state = CSVState::QuotedQuote;
				break;
			default:  fields[i].push_back(c);
				break;
			}
			break;
		case CSVState::QuotedQuote:
			switch (c) {
			case ',': // , after closing quote
				fields.push_back(""); i++;
				state = CSVState::UnquotedField;
				break;
			case '"': // "" -> "
				fields[i].push_back('"');
				state = CSVState::QuotedField;
				break;
			default:  // end of quote
				state = CSVState::UnquotedField;
				break;
			}
			break;
		}
	}
	return fields;
}

condition_variable CV_FillVector;
bool EndConsume = false;

void PrintTable(vector<vector<string>>& table);
void Consume(bool& doneFlag, vector<vector<string>>& Vector, mutex& locker)
{
	unique_lock<mutex> lock(locker);

	vector<vector<string>> readTable;
	int value = 0;
	while (true)
	{
		CV_FillVector.wait(lock);
		if (value < 10)
		{
			for (auto& row : Vector)
			{
				readTable.push_back(row);
			}
			Vector.clear();
			std::sort(readTable.begin(), readTable.end(),
				[](const std::vector<string>& a, const std::vector<string>& b) -> bool {
					return stoi(a[4]) > stoi(b[4]);
				});
			system("cls");
			cout << "Exoplanets" << endl;
			PrintTable(readTable);
			this_thread::sleep_for(chrono::milliseconds(100));
			if (doneFlag)
				return;
		}
	}
	EndConsume = true;
}

string GetDivider(size_t n)
{
	string divider = "";
	for (size_t i = 0; i < n; i++)
		divider += "-";
	return divider;
}

// ������ �������
struct TableHeaderStats {
	string headerTitle;
	int size;
};

const vector<TableHeaderStats> HEADERSTATS = {
	{ "Row"                , 6  },
	{ "Planet Name"        , 30 },
	{ "Host Name"          , 30 },
	{ "Stars"              , 6  },
	{ "Planets"            , 8  },
	{ "Year"               , 5  },
	{ "Orb. Period [days]" , 20 },
	{ "Pl. Mass [Earths]"  , 20 },
	{ "Eccentricity"       , 13 },
	{ "Distance [pc]"      , 14 }
};

int CountTableSize()
{
	int count = 0;
	for (auto& i : HEADERSTATS)
		count += i.size;
	return count;
}

void PrintTable(vector<vector<string>>& table)
{
	for (auto& i : HEADERSTATS)
		cout << left << setw(i.size) << i.headerTitle;
	cout << endl;
	cout << GetDivider(CountTableSize()) << endl;
	for (auto& i : table)
	{
		for (size_t j = 0; j < HEADERSTATS.size(); j++)
			cout << left << setw(HEADERSTATS[j].size) << i[j];
		cout << endl;
	}
}

void Produce(bool& doneFlag, istream& in, vector<vector<string>>& VectorToFill, mutex& locker)
{
	unique_lock<mutex> ProduceWritingUL(locker, defer_lock);
	unique_lock<mutex> ProduceReadingUL(locker, defer_lock);

	vector<vector<string>> table;
	string row;
	vector<string> fields;
	while (!in.eof()) {
		ProduceReadingUL.lock();
		getline(in, row);
		if (in.bad() || in.fail()) {
			break;
		}
		fields = readCSVRow(row);
		ProduceReadingUL.unlock();

		ProduceWritingUL.lock();
		VectorToFill.push_back(fields);
		ProduceWritingUL.unlock();

		this_thread::sleep_for(chrono::milliseconds(100));

		if (VectorToFill.size() > 50)
		{
			CV_FillVector.notify_one();
		}
	}
	doneFlag = true;
}

int main() {
	::SendMessage(::GetConsoleWindow(), WM_SYSKEYDOWN, VK_RETURN, 0x20000000);
	ifstream planetTable;
	planetTable.open("PS_2023.01.04_06.47.18.csv");

	vector<vector<string>> myVector;
	vector<thread> threads;
	mutex m;
	mutex& lockerForMyVector = m;

	bool readIsDone = false;

	for (size_t i = 0; i < 2; i++)
		threads.push_back(thread([&](){ Produce(readIsDone, planetTable, myVector, lockerForMyVector); }));
	for (auto& t : threads)
		t.detach();

	thread ConsumeThread = thread([&](){ Consume(readIsDone, myVector, lockerForMyVector); });
	ConsumeThread.join();

	system("pause");
	return 0;
}
